﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StackController : MonoBehaviour
{
    // Start is called before the first frame update
    public int currentStack = 0;
    float offset = 0.13f;
    GameObject fx1;
    private void Awake()
    {

    }

    public Vector3 getTopPosition()
    {
        return new Vector3(transform.position.x, transform.position.y + (offset * currentStack), transform.position.z);
    }
  

    public void addToStack(GameObject temp)
    {
        temp.transform.position = new Vector3(temp.transform.position.x, temp.transform.position.y + (offset*currentStack), temp.transform.position.z);
        currentStack++;
        Instantiate(GameObject.Find("GameController").GetComponent<GameController>().fx1, temp.transform.position, Quaternion.identity);
    }
}
