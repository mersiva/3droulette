﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public List<GameObject> chipModel = new List<GameObject>();
    public GameObject currentChipMode;

    public GameObject chipGroup;

    public delegate void OnGameReset();
    public static OnGameReset onGameReset;

    public delegate void OnSpinRoulette(int index);
    public static OnSpinRoulette onSpinRoulette;

    public GameObject fx1;


    public GameObject spawnChip(Vector3 pos)
    {
        GameObject temp = Instantiate(currentChipMode, pos, Quaternion.identity);

        temp.transform.SetParent(chipGroup.transform,true);
        return temp;
    }

    public void resetGame()
    {
        foreach(Transform c in chipGroup.transform)
        {
            Destroy(c.gameObject);
        }

        if(onGameReset!=null)
            onGameReset();
    }

    public void spinRoulette(int index)
    {
        if(onSpinRoulette!=null)
        {
            onSpinRoulette(index);
        }
    }

    private void OnDestroy()
    {
        onGameReset = null;
        onSpinRoulette = null;
    }
}
