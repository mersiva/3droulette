﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BetPositions : MonoBehaviour
{
    public List<GameObject> AllBetsPosition;
    // Start is called before the first frame update
    
    [ContextMenu("UpdateAllBetsPosition")]
    public void GetAllBetsPosition()
    {
        AllBetsPosition = new List<GameObject>();
        foreach (Transform c in transform)
        {
            GameObject[] temp = c.GetComponent<BetCommand>().chipPositions.ToArray();
            AllBetsPosition.AddRange(temp);
        }
    }

    private void OnEnable()
    {
        GameController.onGameReset += resetStack;
    }

    private void OnDisable()
    {
        GameController.onGameReset -= resetStack;
    }

    void resetStack()
    {
        Debug.LogError("reseting in betpositions");
        foreach (Transform g in transform)
        {
            foreach(Transform p in g )
            {
                if (p.GetComponent<StackController>())
                {
                    p.GetComponent<StackController>().currentStack = 0;
                    Debug.LogError("reseting stack");
                }
            }
           
        }
    }
}
