﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class RouletteSpin : MonoBehaviour
{


    public delegate void OnRouletteSpin(GameObject g);
    public static OnRouletteSpin onRouletteSpin;

    public GameObject numberPosition;



    private void OnEnable()
    {
        GameController.onSpinRoulette += spin;
    }

    private void OnDisable()
    {
        GameController.onSpinRoulette -= spin;
    }

    void spin(int index)
    {
 
        if (onRouletteSpin != null)
        {
            int no = 0;
            foreach(Transform c in numberPosition.transform)
            {
                if(int.Parse(c.gameObject.name) == index )
                {

                    onRouletteSpin(numberPosition.transform.GetChild(no).gameObject);
                    break;
                }
                no++;
            }
        
        }
      
        //Sequence mySequence = DOTween.Sequence();
        //mySequence.Append(transform.DORotate(new Vector3(0, 180*15, 0), 15, RotateMode.FastBeyond360).SetEase(Ease.OutCubic).OnComplete(()=>
        //{

        //}));
    }

    private void Update()
    {
        transform.Rotate(Vector3.up * (25 * Time.deltaTime));
    }


    private void OnDestroy()
    {

    }

}
