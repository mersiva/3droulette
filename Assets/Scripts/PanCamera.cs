﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PanCamera : MonoBehaviour
{
    DOTweenPath path;

    private void OnEnable()
    {
        path.DORestart();
        path.DOPlay();
    }

    private void OnDisable()
    {
        
    }

    private void Awake()
    {
        path = GetComponent<DOTweenPath>();
    }
    
    public void playCamera()
    {
       
    }
}
