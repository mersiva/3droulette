﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class RouletteBall : MonoBehaviour
{
    public delegate void OnBallStopped(string number);
    public static OnBallStopped onBallStopped;


    Rigidbody rb;
    public GameObject currentTarget;

    public Vector3 startingPos;

    bool moveToTarget = false;
    DOTweenPath path; 
    private void OnEnable()
    {

        RouletteSpin.onRouletteSpin += startPath;
    }

    private void OnDisable()
    {
       
        RouletteSpin.onRouletteSpin -= startPath;
    }

    private void Awake()
    {
        path = GetComponent<DOTweenPath>();
        rb = GetComponent<Rigidbody>();
    }

    void startPath(GameObject target)
    {

        transform.position = startingPos;
        path.DORestart();
        path.DOPlay();
        currentTarget = target;
        moveToTarget = false;
    }

    public void attractBall()
    {
        Debug.Log("path complete");
        moveToTarget = true;
    }

    private void Update()
    {
  

        if (moveToTarget && currentTarget != null && Vector3.Distance(transform.position, currentTarget.transform.position) > 0.25f)
        {
            Debug.LogError("moving to target");
            Vector3 direction = (currentTarget.transform.position - transform.position).normalized;

            rb.AddForce(direction * 0.5f,ForceMode.Impulse);
  
        }
        else if (moveToTarget && currentTarget != null)
        {
            rb.velocity = Vector3.zero;
            transform.position = currentTarget.transform.position;
            moveToTarget = false;

            if (onBallStopped != null)
                onBallStopped(currentTarget.gameObject.name);

            currentTarget = null;
        }
       
    }
}
