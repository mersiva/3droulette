﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class CameraController : MonoBehaviour
{
    public List<GameObject> cameraList = new List<GameObject>();
    // Start is called before the first frame update

    public int index = 0;
    public GameObject currentCamera;

    public float fov;
    public float dof;

    public PostProcessProfile pp;

    public void setCam(int index)
    {
        currentCamera.SetActive(false);
        if (index < cameraList.Count)
            currentCamera = cameraList[index];

        currentCamera.SetActive(true);

    }

    public void checkFov(float s)
    {
        foreach(GameObject g in cameraList)
        {
            g.GetComponent<Camera>().fieldOfView = s;
        }
        fov = s;
    }

    public void checkDof(float s)
    {
        DepthOfField m_DepthOfFeild = null;
        pp.TryGetSettings(out m_DepthOfFeild);
        m_DepthOfFeild.focusDistance.Override(s);
        dof = s;
    }


    private void Awake()
    {
        currentCamera = Camera.main.gameObject;
    }
    [ContextMenu("Next")]
    public void nextCamera()
    {
        if(index < cameraList.Count-1)
        {
            index++;
          
        }
        else
        {
            index = 0;
        }

        currentCamera.SetActive(false);
        currentCamera = cameraList[index];
        currentCamera.SetActive(true);
    }
}
