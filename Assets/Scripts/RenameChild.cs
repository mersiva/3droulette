﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenameChild : MonoBehaviour
{
   
    [ContextMenu("Rename")]
    public void rename()
    {
        int index = 0;
        foreach(Transform child in transform)
        {
            child.gameObject.name = index.ToString();
            index++;
        }
    }


}
