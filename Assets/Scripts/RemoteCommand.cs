﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;


public class RemoteCommand : MonoBehaviour
{
    public GameController con;
    public BetPositions betPos;
    public CameraController camCon;

    public void receiveString(string s)
    {
   
        if (s.Split(':').Length > 1)
        {

            if (s.Split(':')[0].Trim() == "Spin")
            {

                con.spinRoulette(int.Parse(s.Split(':')[1].Trim()));
            }
            else if (s.Split(':')[0].Trim() == "Fov")
            {
                camCon.checkFov(float.Parse(s.Split(':')[1].Trim()));
            }
            else if (s.Split(':')[0].Trim() == "Dof")
            {
                camCon.checkDof(float.Parse(s.Split(':')[1].Trim()));
            }
            else if (s.Split(':')[0].Trim() == "Cam")
            {
                camCon.setCam(int.Parse(s.Split(':')[1].Trim()));
            }
            //Player : bet command
            else if (int.Parse(s.Split(':')[0].Trim()) < con.chipModel.Count)
            {
                con.currentChipMode = con.chipModel[int.Parse(s.Split(':')[0].Trim())];
                Debug.LogError("Here ? ");
                if (int.Parse(s.Split(':')[1].Trim()) < betPos.AllBetsPosition.Count)
                {
                    GameObject temp = con.spawnChip(betPos.AllBetsPosition[int.Parse(s.Split(':')[1].Trim())].transform.position);
                    Debug.LogError(">" + s.Split(':')[2].Trim() + "<");
                    temp.transform.GetChild(0).GetComponent<TextMeshPro>().text = s.Split(':')[2].Trim();
                    betPos.AllBetsPosition[int.Parse(s.Split(':')[1].Trim())].GetComponent<StackController>().addToStack(temp);
                }

            }

        }
        else if (s == "New Game")
        {
            Debug.LogError("reset now");
            con.resetGame();
        }
        else if (s == "ChangeCamera")
        {
            Debug.LogError("reset now");
            camCon.nextCamera();
        }
      

    }



        public void Action_ProcessByteData(byte[] _byte)
        {
            if (FMNetworkManager.instance.NetworkType == FMNetworkType.Server)
            {


            }

        }
}
