﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerController : MonoBehaviour
{
    public GameObject markerPrefab;
    public GameObject currentMarker;
    // Start is called before the first frame update

    public GameObject ballPosition;

    private void OnEnable()
    {
        RouletteBall.onBallStopped += moveMarker;
        GameController.onSpinRoulette += resetMarker;
        GameController.onGameReset += resetMarker;
    }

    private void OnDisable()
    {
        RouletteBall.onBallStopped -= moveMarker;
        GameController.onSpinRoulette -= resetMarker;
        GameController.onGameReset -= resetMarker;
    }
    void resetMarker()
    {
        resetMarker(0);
    }
    void resetMarker(int index)
    {
        currentMarker.transform.position = transform.position;
    }
    void moveMarker(string stack)
    {
        foreach(Transform child in ballPosition.transform)
        {
            if(child.gameObject.name == stack)
            {
                currentMarker.transform.position = child.GetComponent<StackController>().getTopPosition();
            }
        }
    }

    private void Awake()
    {
        currentMarker = Instantiate(markerPrefab, transform.position, Quaternion.identity);
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
